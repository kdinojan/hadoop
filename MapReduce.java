package com.company;

import java.util.ArrayList;

public class MapReduce{

    public static void main(String args[]) {

        // Matrix to transpose
        //On génère une matrice 3x3 qu'on veut transposer par la suite
        int[][] matrix = new int[3][3];
        for (int i=0;i<3;i++)
        {
            for(int j=0; j<3; j++) {
                matrix[i][j] = (3 * i) + j; //remplissage de la matrice
                //System.out.print(matrix[i][j]+" ");
            }
            //System.out.print("\n");
            /*
            0 1 2
            3 4 5
            6 7 8
             */
        }

        int inkeys[] = new int[2];

        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++) {
                inkeys[0]=i; //ligne
                inkeys[1]=j; //colonne

                //mapping
                //on donne une valeur de la matrice ayant pour clé sa position (ligne,colonne)
                //Ici, map ne retourne rien même si il devrait, sur hadoop le context.write est utilisé
                map(matrix[i][j],inkeys);

            }
        }


        //reducing
        //pas besoin pour le cas d'une transposition de matrice
    }

    //Fonction map qui prend en entree une cle et une valeur et
    // retourne un nouveau couple (cle,valeur)
    //la cle sera un couple d'indice (ligne,colonne) indiquant la position
    // de la valeur dans la matrice d'origine
    //map(valeur, ligne et colonne de la valeur)
    public static void map(int inValue, int[] inkeys)
    {
        int[] outkeys = new int[2];
        //modification de la clé d'entrée
        //on echange les indices
        outkeys[0] = inkeys[1];
        outkeys[1] = inkeys[0];
        int outValue = inValue; // on ne modifie pas la valeur d'entree

        //sur hadoop nous ferions un context.write
        //ici nous sommes obligés de retourner le résultat

        //sortie :
        //(valeur, nouvelle clé: ligne et colonne inversées)
    }

    public static void reduce(ArrayList<Integer> valueList, int[] outkey)
    {
        //Dans l'exemple de transposition d'une matrice, il n'y a pas besoin d'opération de reducing,
        // le mapping est suffisant.
    }




}
